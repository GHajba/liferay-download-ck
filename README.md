[![Contact me on Codementor](https://cdn.codementor.io/badges/contact_me_github.svg)](https://www.codementor.io/ghajba?utm_source=github&utm_medium=button&utm_term=ghajba&utm_campaign=github)

Liferay CKEditor Plugin Hook
============================

This is a simple CKEditor plugin which creates a download content for Liferay sites. It uses Lifera's Hook mechanism to alter and add the required files.

(C) 2014-2016 JaPy Szoftver Kft
Licensing: see LICENSE